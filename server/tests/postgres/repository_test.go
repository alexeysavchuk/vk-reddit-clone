package repository_test

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	"github.com/stretchr/testify/assert"
	repository "gitlab.com/alexeysavchuk/vk-reddit-clone/internal/adapters/repository/postgres"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/ports"
)

const (
	user     = "user"
	password = "password"
	dbName   = "db"
)

var db *sql.DB
var migration *migrate.Migrate

func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not construct pool: %s", err)
	}

	err = pool.Client.Ping()
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	resource, err := pool.RunWithOptions(
		&dockertest.RunOptions{
			Repository: "postgres",
			Tag:        "16",
			Env: []string{
				fmt.Sprintf("POSTGRES_USER=%s", user),
				fmt.Sprintf("POSTGRES_PASSWORD=%s", password),
				fmt.Sprintf("POSTGRES_DB=%s", dbName),
			},
		},
		func(config *docker.HostConfig) {
			config.AutoRemove = true
			config.RestartPolicy = docker.RestartPolicy{Name: "no"}
		},
	)
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	hostAndPort := resource.GetHostPort("5432/tcp")
	databaseUrl := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		user,
		password,
		hostAndPort,
		dbName,
	)

	log.Println("Connecting to database on url: ", databaseUrl)

	resource.Expire(120)

	pool.MaxWait = 120 * time.Second
	if err = pool.Retry(func() error {
		db, err = sql.Open("postgres", databaseUrl)
		if err != nil {
			return err
		}
		return db.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	err = resource.Expire(600)
	if err != nil {
		log.Fatalf("Could not expire resource: %s", err)
	}

	migration, err = migrate.New("file://../../migrations", databaseUrl)
	if err != nil {
		log.Fatalf("Could not create migration instance: %s", err)
	}

	code := m.Run()

	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func migrateUp() {
	if err := migration.Up(); err != nil && err != migrate.ErrNoChange {
		log.Fatalf("failed to run up migration: %s", err)
	}
}

func migrateDown() {
	if err := migration.Down(); err != nil && err != migrate.ErrNoChange {
		log.Fatalf("failed to run down migration: %s", err)
	}
}

func TestGetUser(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	user := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdUser, err := repo.CreateUser(context.Background(), user)
	if err != nil {
		t.Fatal(err)
	}
	if createdUser == nil {
		t.Fatal("no created user returned")
	}
	assert.Equal(t, user.Username, createdUser.Username)
	assert.Equal(t, user.Password, createdUser.Password)

	foundUser, err := repo.GetUser(context.Background(), createdUser.UserID)
	if err != nil {
		t.Fatal(err)
	}
	if foundUser == nil {
		t.Fatal("no found user returned")
	}
	assert.Equal(t, createdUser.UserID, foundUser.UserID)
	assert.Equal(t, createdUser.Username, foundUser.Username)
	assert.Equal(t, createdUser.Password, foundUser.Password)
}

func TestGetUserByUsername(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	user := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdUser, err := repo.CreateUser(context.Background(), user)
	if err != nil {
		t.Fatal(err)
	}
	if createdUser == nil {
		t.Fatal("no created user returned")
	}
	assert.Equal(t, user.Username, createdUser.Username)
	assert.Equal(t, user.Password, createdUser.Password)

	foundUser, err := repo.GetUserByUsername(context.Background(), createdUser.Username)
	if err != nil {
		t.Fatal(err)
	}
	if foundUser == nil {
		t.Fatal("no found user returned")
	}
	assert.Equal(t, createdUser, foundUser)
}

func TestCreateUser(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	user := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdUser, err := repo.CreateUser(context.Background(), user)
	if err != nil {
		t.Fatal(err)
	}
	if createdUser == nil {
		t.Fatal("no created user returned")
	}
	assert.Equal(t, user.Username, createdUser.Username)
	assert.Equal(t, user.Password, createdUser.Password)
}

func TestGetPost(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	foundPost, err := repo.GetPost(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}
	if foundPost == nil {
		t.Fatal("no found post returned")
	}
	assert.Equal(t, *createdPost, *foundPost)
}

func TestCreatePost(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)
}

func TestDeletePost(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	err = repo.DeletePost(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}

	foundPosts, err := repo.GetAllPosts(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, 0, len(foundPosts))
}

func TestGetAllPosts(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	posts := []*domain.PostDB{
		{
			AuthorID: createdAuthor.UserID,
			Title:    "The Beatles",
			Category: "music",
			Type:     "text",
			Body:     "The Beatles were an English rock band.",
		},
		{
			AuthorID: createdAuthor.UserID,
			Title:    "The Rolling Stones",
			Category: "music",
			Type:     "text",
			Body:     "The Rolling Stones were an English rock band.",
		},
	}

	createdPosts := []domain.PostDB{}
	for _, post := range posts {
		createdPost, err := repo.CreatePost(context.Background(), post)
		if err != nil {
			t.Fatal(err)
		}
		if createdPost == nil {
			t.Fatal("no created post returned")
		}
		assert.Equal(t, post.AuthorID, createdPost.AuthorID)
		assert.Equal(t, post.Title, createdPost.Title)
		assert.Equal(t, post.Category, createdPost.Category)
		assert.Equal(t, post.Type, createdPost.Type)
		assert.Equal(t, post.Body, createdPost.Body)

		createdPosts = append(createdPosts, *createdPost)
	}

	foundPosts, err := repo.GetAllPosts(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	if foundPosts == nil {
		t.Fatal("no found posts returned")
	}

	for _, createdPost := range createdPosts {
		found := false
		for _, foundPost := range foundPosts {
			if createdPost.PostID == foundPost.PostID {
				assert.Equal(t, createdPost, foundPost)

				found = true
				break
			}
		}
		assert.True(t, found)
	}
}

func TestGetAllPostsByCategory(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	posts := []*domain.PostDB{
		{
			AuthorID: createdAuthor.UserID,
			Title:    "The Beatles",
			Category: "music",
			Type:     "text",
			Body:     "The Beatles were an English rock band.",
		},
		{
			AuthorID: createdAuthor.UserID,
			Title:    "Chat GPT-4",
			Category: "machine learning",
			Type:     "text",
			Body:     "Chat GPT-4 is a powerful artificial intelligence language model.",
		},
	}

	createdPosts := []domain.PostDB{}
	for _, post := range posts {
		createdPost, err := repo.CreatePost(context.Background(), post)
		if err != nil {
			t.Fatal(err)
		}
		if createdPost == nil {
			t.Fatal("no created post returned")
		}
		assert.Equal(t, post.AuthorID, createdPost.AuthorID)
		assert.Equal(t, post.Title, createdPost.Title)
		assert.Equal(t, post.Category, createdPost.Category)
		assert.Equal(t, post.Type, createdPost.Type)
		assert.Equal(t, post.Body, createdPost.Body)

		createdPosts = append(createdPosts, *createdPost)
	}

	foundPosts, err := repo.GetAllPostsByCategory(context.Background(), "music")
	if err != nil {
		t.Fatal(err)
	}
	if foundPosts == nil {
		t.Fatal("no found posts returned")
	}
	assert.Equal(t, 1, len(foundPosts))
	assert.Equal(t, createdPosts[0], foundPosts[0])

	foundPosts, err = repo.GetAllPostsByCategory(context.Background(), "machine learning")
	if err != nil {
		t.Fatal(err)
	}
	if foundPosts == nil {
		t.Fatal("no found posts returned")
	}
	assert.Equal(t, 1, len(foundPosts))
	assert.Equal(t, createdPosts[1], foundPosts[0])
}

func TestGetAllPostsByUser(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	authors := []domain.UserDB{
		{
			Username: "j0hn_d0e",
			Password: "h4shedp4ssw0rd",
		},
		{
			Username: "m4ria_r1ta",
			Password: "p4ssw0rdh4shed",
		},
	}

	createdAuthors := []domain.UserDB{}
	for _, author := range authors {
		createdAuthor, err := repo.CreateUser(context.Background(), &author)
		if err != nil {
			t.Fatal(err)
		}
		if createdAuthor == nil {
			t.Fatal("no created author returned")
		}
		assert.Equal(t, author.Username, createdAuthor.Username)
		assert.Equal(t, author.Password, createdAuthor.Password)

		createdAuthors = append(createdAuthors, *createdAuthor)
	}

	posts := []*domain.PostDB{
		{
			AuthorID: createdAuthors[0].UserID,
			Title:    "The Beatles",
			Category: "music",
			Type:     "text",
			Body:     "The Beatles were an English rock band.",
		},
		{
			AuthorID: createdAuthors[1].UserID,
			Title:    "Chat GPT-4",
			Category: "machine learning",
			Type:     "text",
			Body:     "Chat GPT-4 is a powerful artificial intelligence language model.",
		},
	}

	createdPosts := []domain.PostDB{}
	for _, post := range posts {
		createdPost, err := repo.CreatePost(context.Background(), post)
		if err != nil {
			t.Fatal(err)
		}
		if createdPost == nil {
			t.Fatal("no created post returned")
		}
		assert.Equal(t, post.AuthorID, createdPost.AuthorID)
		assert.Equal(t, post.Title, createdPost.Title)
		assert.Equal(t, post.Category, createdPost.Category)
		assert.Equal(t, post.Type, createdPost.Type)
		assert.Equal(t, post.Body, createdPost.Body)

		createdPosts = append(createdPosts, *createdPost)
	}

	foundPosts, err := repo.GetAllPostsByUser(context.Background(), createdAuthors[0].UserID)
	if err != nil {
		t.Fatal(err)
	}
	if foundPosts == nil {
		t.Fatal("no found posts returned")
	}
	assert.Equal(t, 1, len(foundPosts))
	assert.Equal(t, createdPosts[0], foundPosts[0])

	foundPosts, err = repo.GetAllPostsByUser(context.Background(), createdAuthors[1].UserID)
	if err != nil {
		t.Fatal(err)
	}
	if foundPosts == nil {
		t.Fatal("no found posts returned")
	}
	assert.Equal(t, 1, len(foundPosts))
	assert.Equal(t, createdPosts[1], foundPosts[0])
}

func TestIncrementViews(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	err = repo.IncrementPostViews(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}

	foundPost, err := repo.GetPost(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}
	if foundPost == nil {
		t.Fatal("no found post returned")
	}
	assert.Equal(t, createdPost.PostID, foundPost.PostID)
	assert.Equal(t, createdPost.AuthorID, foundPost.AuthorID)
	assert.Equal(t, createdPost.Title, foundPost.Title)
	assert.Equal(t, createdPost.Category, foundPost.Category)
	assert.Equal(t, createdPost.Type, foundPost.Type)
	assert.Equal(t, createdPost.Body, foundPost.Body)
	assert.Equal(t, createdPost.Views+1, foundPost.Views)
	assert.Equal(t, createdPost.CreatedAt, foundPost.CreatedAt)
}

func TestGetPostComment(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	comment := &domain.CommentDB{
		AuthorID: createdAuthor.UserID,
		PostID:   createdPost.PostID,
		Body:     "I love the Beatles!",
	}
	createdComment, err := repo.CreatePostComment(context.Background(), comment)
	if err != nil {
		t.Fatal(err)
	}
	if createdComment == nil {
		t.Fatal("no created comment returned")
	}
	assert.Equal(t, comment.AuthorID, createdComment.AuthorID)
	assert.Equal(t, comment.PostID, createdComment.PostID)
	assert.Equal(t, comment.Body, createdComment.Body)

	foundComment, err := repo.GetPostComment(context.Background(), createdComment.CommentID)
	if err != nil {
		t.Fatal(err)
	}
	if foundComment == nil {
		t.Fatal("no found comment returned")
	}
	assert.Equal(t, createdComment, foundComment)
}

func TestGetPostComments(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text	",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	comments := []*domain.CommentDB{
		{
			PostID:   createdPost.PostID,
			AuthorID: createdAuthor.UserID,
			Body:     "I love the Beatles.",
		},
		{
			PostID:   createdPost.PostID,
			AuthorID: createdAuthor.UserID,
			Body:     "I hate the Rolling Stones.",
		},
	}

	var createdComments []domain.CommentDB
	for _, comment := range comments {
		createdComment, err := repo.CreatePostComment(context.Background(), comment)
		if err != nil {
			t.Fatal(err)
		}
		if createdComment == nil {
			t.Fatal("no created comment returned")
		}
		assert.Equal(t, comment.PostID, createdComment.PostID)
		assert.Equal(t, comment.AuthorID, createdComment.AuthorID)
		assert.Equal(t, comment.Body, createdComment.Body)

		createdComments = append(createdComments, *createdComment)
	}

	foundComments, err := repo.GetPostComments(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}
	if foundComments == nil {
		t.Fatal("no found comments returned")
	}

	for _, createdComment := range createdComments {
		found := false
		for _, foundComment := range foundComments {
			if createdComment.CommentID == foundComment.CommentID {
				assert.Equal(t, createdComment, foundComment)

				found = true
				break
			}
		}
		assert.True(t, found)
	}
}

func TestCreatePostComment(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	comment := &domain.CommentDB{
		PostID:   createdPost.PostID,
		AuthorID: createdAuthor.UserID,
		Body:     "I hate the Rolling Stones.",
	}
	createdComment, err := repo.CreatePostComment(context.Background(), comment)
	if err != nil {
		t.Fatal(err)
	}
	if createdComment == nil {
		t.Fatal("no created comment returned")
	}
	assert.Equal(t, comment.PostID, createdComment.PostID)
	assert.Equal(t, comment.AuthorID, createdComment.AuthorID)
	assert.Equal(t, comment.Body, createdComment.Body)
}

func TestDeletePostComment(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	comment := &domain.CommentDB{
		PostID:   createdPost.PostID,
		AuthorID: createdAuthor.UserID,
		Body:     "I hate the Rolling Stones.",
	}
	createdComment, err := repo.CreatePostComment(context.Background(), comment)
	if err != nil {
		t.Fatal(err)
	}
	if createdComment == nil {
		t.Fatal("no created comment returned")
	}
	assert.Equal(t, comment.PostID, createdComment.PostID)
	assert.Equal(t, comment.AuthorID, createdComment.AuthorID)
	assert.Equal(t, comment.Body, createdComment.Body)

	err = repo.DeletePostComment(context.Background(), createdComment.CommentID)
	if err != nil {
		t.Fatal(err)
	}

	comments, err := repo.GetPostComments(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, 0, len(comments))
}

func TestGetPostVotes(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	authors := []*domain.UserDB{
		{
			Username: "j0hn_d0e",
			Password: "h4shedp4ssw0rd",
		},
		{
			Username: "m4ria_r1ta",
			Password: "p4ssw0rdh4shed",
		},
	}

	createdAuthors := []domain.UserDB{}
	for _, author := range authors {
		createdAuthor, err := repo.CreateUser(context.Background(), author)
		if err != nil {
			t.Fatal(err)
		}
		if createdAuthor == nil {
			t.Fatal("no created author returned")
		}
		assert.Equal(t, author.Username, createdAuthor.Username)
		assert.Equal(t, author.Password, createdAuthor.Password)

		createdAuthors = append(createdAuthors, *createdAuthor)
	}

	post := &domain.PostDB{
		AuthorID: createdAuthors[0].UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text	",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	err = repo.UpvotePost(context.Background(), createdPost.PostID, createdAuthors[0].UserID)
	if err != nil {
		t.Fatal(err)
	}

	err = repo.DownvotePost(context.Background(), createdPost.PostID, createdAuthors[1].UserID)
	if err != nil {
		t.Fatal(err)
	}

	votes, err := repo.GetPostVotes(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}
	if votes == nil {
		t.Fatal("no votes returned")
	}

	assert.Equal(t, 2, len(votes))

	switch {
	case votes[0].UserID == createdAuthors[0].UserID && votes[0].UserID != createdAuthors[1].UserID:
		assert.Equal(t, 1, votes[0].Value)
	case votes[0].UserID == createdAuthors[1].UserID && votes[0].UserID != createdAuthors[0].UserID:
		assert.Equal(t, -1, votes[0].Value)
	default:
		t.Fatal("invalid vote")
	}
}

func TestUpvotePost(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text	",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	err = repo.UpvotePost(context.Background(), createdPost.PostID, createdAuthor.UserID)
	if err != nil {
		t.Fatal(err)
	}

	votes, err := repo.GetPostVotes(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}
	if votes == nil {
		t.Fatal("no votes returned")
	}

	assert.Equal(t, 1, len(votes))
	assert.Equal(t, 1, votes[0].Value)
}

func TestDownvotePost(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text	",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	err = repo.DownvotePost(context.Background(), createdPost.PostID, createdAuthor.UserID)
	if err != nil {
		t.Fatal(err)
	}

	votes, err := repo.GetPostVotes(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}
	if votes == nil {
		t.Fatal("no votes returned")
	}

	assert.Equal(t, 1, len(votes))
	assert.Equal(t, -1, votes[0].Value)
}

func TestUnvotePost(t *testing.T) {
	migrateUp()
	defer migrateDown()

	repo := repository.NewAppRepository(db)

	author := &domain.UserDB{
		Username: "j0hn_d0e",
		Password: "h4shedp4ssw0rd",
	}
	createdAuthor, err := repo.CreateUser(context.Background(), author)
	if err != nil {
		t.Fatal(err)
	}
	if createdAuthor == nil {
		t.Fatal("no created author returned")
	}
	assert.Equal(t, author.Username, createdAuthor.Username)
	assert.Equal(t, author.Password, createdAuthor.Password)

	post := &domain.PostDB{
		AuthorID: createdAuthor.UserID,
		Title:    "The Beatles",
		Category: "music",
		Type:     "text	",
		Body:     "The Beatles were an English rock band.",
	}
	createdPost, err := repo.CreatePost(context.Background(), post)
	if err != nil {
		t.Fatal(err)
	}
	if createdPost == nil {
		t.Fatal("no created post returned")
	}
	assert.Equal(t, post.AuthorID, createdPost.AuthorID)
	assert.Equal(t, post.Title, createdPost.Title)
	assert.Equal(t, post.Category, createdPost.Category)
	assert.Equal(t, post.Type, createdPost.Type)
	assert.Equal(t, post.Body, createdPost.Body)

	err = repo.UpvotePost(context.Background(), createdPost.PostID, createdAuthor.UserID)
	if err != nil {
		t.Fatal(err)
	}

	err = repo.UnvotePost(context.Background(), createdPost.PostID, createdAuthor.UserID)
	if err != nil {
		t.Fatal(err)
	}

	votes, err := repo.GetPostVotes(context.Background(), createdPost.PostID)
	if err != nil {
		t.Fatal(err)
	}
	if votes == nil {
		t.Fatal("no votes returned")
	}

	assert.Equal(t, 0, len(votes))
}

func TestTransactionCommit(t *testing.T) {
	migrateUp()
	defer migrateDown()

	atomicRepo := repository.NewAtomicAppRepository(db)

	var createdUser *domain.UserDB
	err := atomicRepo.Execute(
		context.Background(),
		func(ctx context.Context, repo ports.AppRepository) error {
			user := &domain.UserDB{
				Username: "j0hn_d0e",
				Password: "h4shedp4ssw0rd",
			}
			var err error
			createdUser, err = repo.CreateUser(ctx, user)
			if err != nil {
				return err
			}
			assert.Equal(t, user.Username, createdUser.Username)
			assert.Equal(t, user.Password, createdUser.Password)

			return nil
		},
	)
	if err != nil {
		t.Fatal(err)
	}

	repo := repository.NewAppRepository(db)

	foundUser, err := repo.GetUser(context.Background(), createdUser.UserID)
	if err != nil {
		t.Fatal(err)
	}
	if foundUser == nil {
		t.Fatal("no found user returned")
	}

	assert.Equal(t, createdUser, foundUser)
}

func TestTransactionRollback(t *testing.T) {
	migrateUp()
	defer migrateDown()

	atomicRepo := repository.NewAtomicAppRepository(db)

	var createdUser *domain.UserDB
	err := atomicRepo.Execute(
		context.Background(),
		func(ctx context.Context, repo ports.AppRepository) error {
			user := &domain.UserDB{
				Username: "j0hn_d0e",
				Password: "h4shedp4ssw0rd",
			}
			var err error
			createdUser, err = repo.CreateUser(ctx, user)
			if err != nil {
				return err
			}
			return fmt.Errorf("error while performing transaction")
		},
	)
	if err == nil || !errors.Is(err, ports.ErrTransactionFailed) {
		t.Fatal("transaction should have failed")
	}

	repo := repository.NewAppRepository(db)

	foundUser, err := repo.GetUser(context.Background(), createdUser.UserID)
	if err == nil {
		t.Fatal("error should have occurred")
	}
	if foundUser != nil {
		t.Fatal("found user returned")
	}
}
