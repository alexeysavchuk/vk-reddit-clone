package service_test

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/google/uuid"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	"github.com/stretchr/testify/assert"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
)

const (
	user     = "user"
	password = "password"
	dbName   = "db"
)

var db *sql.DB
var migration *migrate.Migrate

func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not construct pool: %s", err)
	}

	err = pool.Client.Ping()
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	resource, err := pool.RunWithOptions(
		&dockertest.RunOptions{
			Repository: "postgres",
			Tag:        "16",
			Env: []string{
				fmt.Sprintf("POSTGRES_USER=%s", user),
				fmt.Sprintf("POSTGRES_PASSWORD=%s", password),
				fmt.Sprintf("POSTGRES_DB=%s", dbName),
			},
		},
		func(config *docker.HostConfig) {
			config.AutoRemove = true
			config.RestartPolicy = docker.RestartPolicy{Name: "no"}
		},
	)
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	hostAndPort := resource.GetHostPort("5432/tcp")
	databaseUrl := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		user,
		password,
		hostAndPort,
		dbName,
	)

	log.Println("Connecting to database on url: ", databaseUrl)

	resource.Expire(120)

	pool.MaxWait = 120 * time.Second
	if err = pool.Retry(func() error {
		db, err = sql.Open("postgres", databaseUrl)
		if err != nil {
			return err
		}
		return db.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	err = resource.Expire(600)
	if err != nil {
		log.Fatalf("Could not expire resource: %s", err)
	}

	migration, err = migrate.New("file://../../migrations", databaseUrl)
	if err != nil {
		log.Fatalf("Could not create migration instance: %s", err)
	}

	code := m.Run()

	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func migrateUp() {
	if err := migration.Up(); err != nil && err != migrate.ErrNoChange {
		log.Fatalf("failed to run up migration: %s", err)
	}
}

func migrateDown() {
	if err := migration.Down(); err != nil && err != migrate.ErrNoChange {
		log.Fatalf("failed to run down migration: %s", err)
	}
}

func TestGetPostViewsIncrement(t *testing.T) {
	migrateUp()
	defer migrateDown()

	testData, err := newTestData(1, 1, 0, 0)
	if err != nil {
		t.Fatalf("failed to create test data: %s", err)
	}

	err = insertTestData(testData, db)
	if err != nil {
		t.Fatalf("failed to insert test data: %s", err)
	}
	var postDB *domain.PostDB
	for _, p := range testData.posts {
		postDB = p
		break
	}

	service := newAppService(db)

	for i := 1; i < 100; i++ {
		post, err := service.GetPost(context.Background(), postDB.PostID)
		if err != nil {
			t.Fatalf("failed to get post: %s", err)
		}
		assert.Equal(t, post.PostID, postDB.PostID.String())
		assert.Equal(t, post.Views, postDB.Views+i)
	}
}

func TestGetAllPosts(t *testing.T) {
	migrateUp()
	defer migrateDown()

	testData, err := newTestData(10, 100, 0, 0)
	if err != nil {
		t.Fatalf("failed to create test data: %s", err)
	}

	err = insertTestData(testData, db)
	if err != nil {
		t.Fatalf("failed to insert test data: %s", err)
	}

	service := newAppService(db)

	posts, err := service.GetAllPosts(context.Background())
	if err != nil {
		t.Fatalf("failed to get posts: %s", err)
	}

	for _, postDB := range testData.posts {
		found := false
		for _, post := range posts {
			if post.PostID == postDB.PostID.String() {
				assert.Equal(t, post.PostID, postDB.PostID.String())
				assert.Equal(t, post.Author.UserID, postDB.AuthorID.String())
				assert.Equal(t, post.Title, postDB.Title)
				assert.Equal(t, post.Category, postDB.Category)
				assert.Equal(t, post.Type, postDB.Type)
				assert.Equal(t, post.Body, postDB.Body)
				assert.Equal(t, post.Views, postDB.Views)

				found = true
				break
			}
		}
		assert.True(t, found)
	}
}

func TestGetAllPostsByCategory(t *testing.T) {
	migrateUp()
	defer migrateDown()

	testData, err := newTestData(10, 1000, 0, 0)
	if err != nil {
		t.Fatalf("failed to create test data: %s", err)
	}

	err = insertTestData(testData, db)
	if err != nil {
		t.Fatalf("failed to insert test data: %s", err)
	}

	var targetCategory string
	for _, p := range testData.posts {
		targetCategory = p.Category
		break
	}

	service := newAppService(db)

	posts, err := service.GetAllPostsByCategory(context.Background(), targetCategory)
	if err != nil {
		t.Fatalf("failed to get posts: %s", err)
	}

	for _, postDB := range testData.posts {
		switch postDB.Category {
		case targetCategory:
			found := false
			for _, post := range posts {
				if post.PostID == postDB.PostID.String() {
					assert.Equal(t, post.PostID, postDB.PostID.String())
					assert.Equal(t, post.Author.UserID, postDB.AuthorID.String())
					assert.Equal(t, post.Title, postDB.Title)
					assert.Equal(t, post.Category, postDB.Category)
					assert.Equal(t, post.Type, postDB.Type)
					assert.Equal(t, post.Body, postDB.Body)
					assert.Equal(t, post.Views, postDB.Views)

					found = true
					break
				}
			}
			assert.True(t, found)
		default:
			found := false
			for _, post := range posts {
				if post.PostID == postDB.PostID.String() {
					found = true
					break
				}
			}
			assert.False(t, found)
		}
	}
}

func TestGetAllPostsByAuthor(t *testing.T) {
	migrateUp()
	defer migrateDown()

	testData, err := newTestData(10, 1000, 0, 0)
	if err != nil {
		t.Fatalf("failed to create test data: %s", err)
	}

	err = insertTestData(testData, db)
	if err != nil {
		t.Fatalf("failed to insert test data: %s", err)
	}

	var authorID uuid.UUID
	for _, p := range testData.posts {
		authorID = p.AuthorID
		break
	}

	service := newAppService(db)

	posts, err := service.GetAllPostsByUser(context.Background(), authorID)
	if err != nil {
		t.Fatalf("failed to get posts: %s", err)
	}

	for _, postDB := range testData.posts {
		switch postDB.AuthorID {
		case authorID:
			found := false
			for _, post := range posts {
				if post.PostID == postDB.PostID.String() {
					assert.Equal(t, post.PostID, postDB.PostID.String())
					assert.Equal(t, post.Author.UserID, postDB.AuthorID.String())
					assert.Equal(t, post.Title, postDB.Title)
					assert.Equal(t, post.Category, postDB.Category)
					assert.Equal(t, post.Type, postDB.Type)
					assert.Equal(t, post.Body, postDB.Body)
					assert.Equal(t, post.Views, postDB.Views)

					found = true
					break
				}
			}
			assert.True(t, found)
		default:
			found := false
			for _, post := range posts {
				if post.PostID == postDB.PostID.String() {
					found = true
					break
				}
			}
			assert.False(t, found)
		}
	}
}

func TestGetAllPostsByUsername(t *testing.T) {
	migrateUp()
	defer migrateDown()

	testData, err := newTestData(10, 1000, 0, 0)
	if err != nil {
		t.Fatalf("failed to create test data: %s", err)
	}

	err = insertTestData(testData, db)
	if err != nil {
		t.Fatalf("failed to insert test data: %s", err)
	}

	var targetUsername string
	for _, p := range testData.posts {
		user := testData.users[p.AuthorID]
		targetUsername = user.Username
		break
	}

	service := newAppService(db)

	posts, err := service.GetAllPostsByUsername(context.Background(), targetUsername)
	if err != nil {
		t.Fatalf("failed to get posts: %s", err)
	}

	for _, postDB := range testData.posts {
		user := testData.users[postDB.AuthorID]
		username := user.Username
		switch username {
		case targetUsername:
			found := false
			for _, post := range posts {
				if post.PostID == postDB.PostID.String() {
					assert.Equal(t, post.PostID, postDB.PostID.String())
					assert.Equal(t, post.Author.UserID, postDB.AuthorID.String())
					assert.Equal(t, post.Title, postDB.Title)
					assert.Equal(t, post.Category, postDB.Category)
					assert.Equal(t, post.Type, postDB.Type)
					assert.Equal(t, post.Body, postDB.Body)
					assert.Equal(t, post.Views, postDB.Views)

					found = true
					break
				}
			}
			assert.True(t, found)
		default:
			found := false
			for _, post := range posts {
				if post.PostID == postDB.PostID.String() {
					found = true
					break
				}
			}
			assert.False(t, found)
		}
	}
}
