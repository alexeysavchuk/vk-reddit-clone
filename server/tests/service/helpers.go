package service_test

import (
	"database/sql"
	"errors"

	"github.com/google/uuid"
	repository "gitlab.com/alexeysavchuk/vk-reddit-clone/internal/adapters/repository/postgres"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/common/faker"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/ports"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/services"
)

type testData struct {
	users    map[uuid.UUID]*domain.UserDB
	posts    map[uuid.UUID]*domain.PostDB
	comments map[uuid.UUID]*domain.CommentDB
	votes    map[uuid.UUID]*domain.VoteDB
}

func newTestData(numUsers, numPosts, numComments, numVotes int) (*testData, error) {
	if numUsers < 0 || numPosts < 0 || numComments < 0 || numVotes < 0 {
		return nil, errors.New("invalid number of users, posts, comments or votes")
	}

	users := make([]*domain.UserDB, 0, numUsers)
	userMap := make(map[uuid.UUID]*domain.UserDB)
	for i := 0; i < numUsers; i++ {
		user := faker.GetFakeUser()
		users = append(users, user)
		userMap[user.UserID] = user
	}

	posts := make([]*domain.PostDB, 0, numPosts)
	postMap := make(map[uuid.UUID]*domain.PostDB)
	for range numPosts {
		author := faker.ChooseRandomUser(users)
		post := faker.GetFakePost(author.UserID)
		posts = append(posts, post)
		postMap[post.PostID] = post
	}

	commentMap := make(map[uuid.UUID]*domain.CommentDB)
	for range numComments {
		post := faker.ChooseRandomPost(posts)
		author := faker.ChooseRandomUser(users)
		comment := faker.GetFakeComment(post.PostID, author.UserID)
		commentMap[comment.CommentID] = comment
	}

	voteMap := make(map[uuid.UUID]*domain.VoteDB)
	for range numVotes {
		post := faker.ChooseRandomPost(posts)
		user := faker.ChooseRandomUser(users)
		vote := faker.GetFakeVote(post.PostID, user.UserID)
		voteMap[vote.VoteID] = vote
	}

	return &testData{
		users:    userMap,
		posts:    postMap,
		comments: commentMap,
		votes:    voteMap,
	}, nil
}

func insertTestData(td *testData, db *sql.DB) error {
	for _, user := range td.users {
		err := insertUser(db, user)
		if err != nil {
			return err
		}
	}

	for _, post := range td.posts {
		err := insertPost(db, post)
		if err != nil {
			return err
		}
	}

	for _, comment := range td.comments {
		err := insertComment(db, comment)
		if err != nil {
			return err
		}
	}

	for _, vote := range td.votes {
		err := insertVote(db, vote)
		if err != nil {
			return err
		}
	}

	return nil
}

func insertUser(db *sql.DB, user *domain.UserDB) error {
	query := `
INSERT INTO
	"user"
	(
		"user_id",
		"username",
		"password"
	)
VALUES
	(
		$1,
		$2,
		$3
	);
`
	_, err := db.Exec(query,
		user.UserID,
		user.Username,
		user.Password,
	)
	return err
}

func insertPost(db *sql.DB, post *domain.PostDB) error {
	query := `
INSERT INTO
	"post"
	(
		"post_id",
		"author_id",
		"title",
		"category",
		"type",
		"body",
		"views",
		"created_at"
	)
VALUES
	(
		$1,
		$2,
		$3,
		$4,
		$5,
		$6,
		$7,
		$8
	);
`
	_, err := db.Exec(query,
		post.PostID,
		post.AuthorID,
		post.Title,
		post.Category,
		post.Type,
		post.Body,
		post.Views,
		post.CreatedAt,
	)
	return err
}

func insertComment(db *sql.DB, comment *domain.CommentDB) error {
	query := `
INSERT INTO
	"comment"
	(
		"comment_id",
		"post_id",
		"author_id",
		"body"
	)
VALUES
	(
		$1,
		$2,
		$3,
		$4
	);
`
	_, err := db.Exec(query,
		comment.CommentID,
		comment.PostID,
		comment.AuthorID,
		comment.Body,
	)
	return err
}

func insertVote(db *sql.DB, vote *domain.VoteDB) error {
	query := `
INSERT INTO
	"vote"
	(
		"vote_id",
		"post_id",
		"user_id",
		"value"
	)
VALUES
	(
		$1,
		$2,
		$3,
		$4
	);
`
	_, err := db.Exec(query,
		vote.VoteID,
		vote.PostID,
		vote.UserID,
		vote.Value,
	)
	return err
}

func newAppService(db *sql.DB) *services.AppService {
	return services.NewAppService(
		func() ports.AppRepository {
			return repository.NewAppRepository(db)
		},
		func() ports.AtomicAppRepository {
			return repository.NewAtomicAppRepository(db)
		},
	)
}
