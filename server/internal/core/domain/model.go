package domain

import (
	"time"

	"github.com/google/uuid"
)

type (
	UserDB struct {
		UserID   uuid.UUID
		Username string
		Password string
	}

	VoteDB struct {
		VoteID uuid.UUID
		PostID uuid.UUID
		UserID uuid.UUID
		Value  int
	}

	CommentDB struct {
		CommentID uuid.UUID
		PostID    uuid.UUID
		AuthorID  uuid.UUID
		Body      string
		CreatedAt time.Time
	}

	PostDB struct {
		PostID    uuid.UUID
		AuthorID  uuid.UUID
		Title     string
		Category  string
		Type      string
		Body      string
		Views     int
		CreatedAt time.Time
	}

	User struct {
		UserID   string
		Username string
	}

	Vote struct {
		UserID string
		Value  int
	}

	Comment struct {
		CommentID string
		Author    User
		Body      string
		CreatedAt time.Time
	}

	Post struct {
		PostID           string
		Author           User
		Title            string
		Category         string
		Type             string
		Body             string
		Views            int
		Comments         []Comment
		Votes            []Vote
		Score            int
		UpvotePercentage int
		CreatedAt        time.Time
	}

	CreateCommentRequest struct {
		PostID   uuid.UUID
		AuthorID uuid.UUID
		Body     string
	}

	CreatePostRequest struct {
		AuthorID uuid.UUID
		Title    string
		Category string
		Type     string
		Body     string
	}
)
