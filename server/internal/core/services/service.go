package services

import (
	"context"
	"fmt"
	"os"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/ports"
	"golang.org/x/crypto/bcrypt"
)

type AppService struct {
	repoFactory       ports.AppRepositoryFactory
	atomicRepoFactory ports.AtomicAppRepositoryFactory
}

func NewAppService(
	repoFactory ports.AppRepositoryFactory,
	atomicRepoFactory ports.AtomicAppRepositoryFactory,
) *AppService {
	return &AppService{
		repoFactory:       repoFactory,
		atomicRepoFactory: atomicRepoFactory,
	}
}

var _ ports.AppService = (*AppService)(nil)

func getPost(ctx context.Context, repo ports.AppRepository, postID uuid.UUID) (*domain.Post, error) {
	postDB, err := repo.GetPost(ctx, postID)
	if err != nil {
		return nil, err
	}

	commentsDB, err := repo.GetPostComments(ctx, postID)
	if err != nil {
		return nil, err
	}

	votesDB, err := repo.GetPostVotes(ctx, postID)
	if err != nil {
		return nil, err
	}

	comments := make([]domain.Comment, 0, len(commentsDB))
	for _, commentDB := range commentsDB {
		authorDB, err := repo.GetUser(ctx, commentDB.AuthorID)
		if err != nil {
			return nil, err
		}

		comment := domain.Comment{
			CommentID: commentDB.CommentID.String(),
			Author: domain.User{
				UserID:   authorDB.UserID.String(),
				Username: authorDB.Username,
			},
			Body:      commentDB.Body,
			CreatedAt: commentDB.CreatedAt,
		}

		comments = append(comments, comment)
	}

	votes := make([]domain.Vote, 0, len(votesDB))
	for _, voteDB := range votesDB {
		vote := domain.Vote{
			UserID: voteDB.UserID.String(),
			Value:  voteDB.Value,
		}

		votes = append(votes, vote)
	}

	author, err := repo.GetUser(ctx, postDB.AuthorID)
	if err != nil {
		return nil, err
	}

	score := 0
	upvotePercentage := 0
	for _, vote := range votes {
		score += vote.Value
		if vote.Value == 1 {
			upvotePercentage++
		}
	}
	if len(votes) > 0 {
		upvotePercentage *= 100
		upvotePercentage /= len(votes)
	}

	post := domain.Post{
		PostID: postDB.PostID.String(),
		Author: domain.User{
			UserID:   author.UserID.String(),
			Username: author.Username,
		},
		Title:            postDB.Title,
		Category:         postDB.Category,
		Type:             postDB.Type,
		Body:             postDB.Body,
		Views:            postDB.Views,
		Comments:         comments,
		Votes:            votes,
		Score:            score,
		UpvotePercentage: upvotePercentage,
		CreatedAt:        postDB.CreatedAt,
	}

	return &post, nil
}

func (s *AppService) GetPost(ctx context.Context, postID uuid.UUID) (*domain.Post, error) {
	repo := s.atomicRepoFactory()

	var post *domain.Post
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			err := repo.IncrementPostViews(ctx, postID)
			if err != nil {
				return err
			}

			post, err = getPost(ctx, repo, postID)
			if err != nil {
				return err
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (s *AppService) CreatePost(ctx context.Context, req *domain.CreatePostRequest) (*domain.Post, error) {
	repo := s.atomicRepoFactory()

	var post *domain.Post
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			postDB, err := repo.CreatePost(ctx, &domain.PostDB{
				AuthorID: req.AuthorID,
				Title:    req.Title,
				Category: req.Category,
				Type:     req.Type,
				Body:     req.Body,
			})
			if err != nil {
				return err
			}

			post, err = getPost(ctx, repo, postDB.PostID)
			if err != nil {
				return err
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (s *AppService) DeletePost(
	ctx context.Context,
	postID uuid.UUID,
	userID uuid.UUID,
) error {
	repo := s.atomicRepoFactory()

	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			postDB, err := repo.GetPost(ctx, postID)
			if err != nil {
				return err
			}

			if postDB.AuthorID != userID {
				return fmt.Errorf("user %s is not the author of post %s", userID, postID)
			}

			return repo.DeletePost(ctx, postID)
		},
	)
	if err != nil {
		return err
	}

	return nil
}

func (s *AppService) GetAllPosts(ctx context.Context) ([]domain.Post, error) {
	repo := s.atomicRepoFactory()

	posts := make([]domain.Post, 0)
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			postsDB, err := repo.GetAllPosts(ctx)
			if err != nil {
				return err
			}

			for _, postDB := range postsDB {
				post, err := getPost(ctx, repo, postDB.PostID)
				if err != nil {
					return err
				}

				posts = append(posts, *post)
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return posts, nil
}

func (s *AppService) GetAllPostsByCategory(ctx context.Context, category string) ([]domain.Post, error) {
	repo := s.atomicRepoFactory()

	posts := make([]domain.Post, 0)
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			postsDB, err := repo.GetAllPostsByCategory(ctx, category)
			if err != nil {
				return err
			}

			for _, postDB := range postsDB {
				post, err := getPost(ctx, repo, postDB.PostID)
				if err != nil {
					return err
				}

				posts = append(posts, *post)
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return posts, nil
}

func (s *AppService) GetAllPostsByUser(ctx context.Context, userID uuid.UUID) ([]domain.Post, error) {
	repo := s.atomicRepoFactory()

	posts := make([]domain.Post, 0)
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			postsDB, err := repo.GetAllPostsByUser(ctx, userID)
			if err != nil {
				return err
			}

			for _, postDB := range postsDB {
				post, err := getPost(ctx, repo, postDB.PostID)
				if err != nil {
					return err
				}

				posts = append(posts, *post)
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return posts, nil
}

func (s *AppService) GetAllPostsByUsername(ctx context.Context, username string) ([]domain.Post, error) {
	atomicRepo := s.atomicRepoFactory()

	posts := make([]domain.Post, 0)
	err := atomicRepo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			postsDB, err := repo.GetAllPostsByUsername(ctx, username)
			if err != nil {
				return err
			}

			for _, postDB := range postsDB {
				post, err := getPost(ctx, repo, postDB.PostID)
				if err != nil {
					return err
				}

				posts = append(posts, *post)
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return posts, nil
}

func (s *AppService) CreatePostComment(ctx context.Context, req *domain.CreateCommentRequest) (*domain.Post, error) {
	repo := s.atomicRepoFactory()

	var post *domain.Post
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			_, err := repo.CreatePostComment(ctx, &domain.CommentDB{
				PostID:   req.PostID,
				AuthorID: req.AuthorID,
				Body:     req.Body,
			},
			)
			if err != nil {
				return err
			}

			post, err = getPost(ctx, repo, req.PostID)
			if err != nil {
				return err
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (s *AppService) DeletePostComment(
	ctx context.Context,
	commentID uuid.UUID,
	userID uuid.UUID,
) (*domain.Post, error) {
	repo := s.atomicRepoFactory()

	var post *domain.Post
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			commentDB, err := repo.GetPostComment(ctx, commentID)
			if err != nil {
				return err
			}

			if commentDB.AuthorID != userID {
				return fmt.Errorf("user %s is not the author of comment %s", userID, commentID)
			}

			err = repo.DeletePostComment(ctx, commentID)
			if err != nil {
				return err
			}

			post, err = getPost(ctx, repo, commentDB.PostID)
			if err != nil {
				return err
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (s *AppService) UpvotePost(ctx context.Context, postID, userID uuid.UUID) (*domain.Post, error) {
	repo := s.atomicRepoFactory()

	var post *domain.Post
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			err := repo.UpvotePost(ctx, postID, userID)
			if err != nil {
				return err
			}

			post, err = getPost(ctx, repo, postID)
			if err != nil {
				return err
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (s *AppService) DownvotePost(ctx context.Context, postID, userID uuid.UUID) (*domain.Post, error) {
	repo := s.atomicRepoFactory()

	var post *domain.Post
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			err := repo.DownvotePost(ctx, postID, userID)
			if err != nil {
				return err
			}

			post, err = getPost(ctx, repo, postID)
			if err != nil {
				return err
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (s *AppService) UnvotePost(ctx context.Context, postID, userID uuid.UUID) (*domain.Post, error) {
	repo := s.atomicRepoFactory()

	var post *domain.Post
	err := repo.Execute(ctx,
		func(ctx context.Context, repo ports.AppRepository) error {
			err := repo.UnvotePost(ctx, postID, userID)
			if err != nil {
				return err
			}

			post, err = getPost(ctx, repo, postID)
			if err != nil {
				return err
			}

			return nil
		},
	)
	if err != nil {
		return nil, err
	}

	return post, nil
}

type AuthService struct {
	repoFactory       ports.AppRepositoryFactory
	atomicRepoFactory ports.AtomicAppRepositoryFactory
}

var _ ports.AuthService = (*AuthService)(nil)

func NewAuthService(
	repoFactory ports.AppRepositoryFactory,
	atomicRepoFactory ports.AtomicAppRepositoryFactory,
) *AuthService {
	return &AuthService{
		repoFactory:       repoFactory,
		atomicRepoFactory: atomicRepoFactory,
	}
}

func (s *AuthService) Register(
	ctx context.Context,
	username string,
	password string,
) (string, error) {
	repo := s.repoFactory()

	hashedPassword, err := bcrypt.GenerateFromPassword(
		[]byte(password),
		bcrypt.DefaultCost,
	)
	if err != nil {
		return "", err
	}

	user, err := repo.CreateUser(ctx, &domain.UserDB{
		Username: username,
		Password: string(hashedPassword),
	})
	if err != nil {
		return "", err
	}

	token, err := s.NewJWT(&domain.User{
		UserID:   user.UserID.String(),
		Username: user.Username,
	})
	if err != nil {
		return "", err
	}

	return token, nil
}

func (s *AuthService) Login(
	ctx context.Context,
	username string,
	password string,
) (string, error) {
	repo := s.repoFactory()

	user, err := repo.GetUserByUsername(ctx, username)
	if err != nil {
		return "", err
	}

	if err := bcrypt.CompareHashAndPassword(
		[]byte(user.Password),
		[]byte(password),
	); err != nil {
		return "", err
	}

	token, err := s.NewJWT(&domain.User{
		UserID:   user.UserID.String(),
		Username: user.Username,
	})
	if err != nil {
		return "", err
	}

	return token, nil
}

func (s *AuthService) NewJWT(user *domain.User) (string, error) {
	tokenObject := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": map[string]interface{}{
			"id":       user.UserID,
			"username": user.Username,
		},
	})

	signingKey := os.Getenv("JWT_SECRET")
	if signingKey == "" {
		return "", fmt.Errorf("%w: JWT_SECRET not set", ports.ErrTokenCreateFailed)
	}

	token, err := tokenObject.SignedString([]byte(signingKey))
	if err != nil {
		return "", fmt.Errorf("%w: %w", ports.ErrTokenCreateFailed, err)
	}

	return token, nil
}

func (s *AuthService) ParseJWT(token string) (*domain.User, error) {
	signingKey := os.Getenv("JWT_SECRET")
	if signingKey == "" {
		return nil, fmt.Errorf("%w: JWT_SECRET not set", ports.ErrTokenParseFailed)
	}

	tokenObject, err := jwt.ParseWithClaims(
		token,
		&jwt.MapClaims{},
		func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf(
					"%w: %s",
					ports.ErrTokenParseFailed,
					"unexpected signing method",
				)
			}

			return []byte(signingKey), nil
		},
	)
	if err != nil {
		return nil, fmt.Errorf("%w: %w", ports.ErrTokenParseFailed, err)
	}

	claims, ok := tokenObject.Claims.(*jwt.MapClaims)
	if !ok {
		return nil, fmt.Errorf(
			"%w: %s",
			ports.ErrTokenParseFailed,
			"invalid token claims",
		)
	}

	user := (*claims)["user"].(map[string]interface{})

	id := user["id"].(string)
	username := user["username"].(string)

	return &domain.User{
		UserID:   id,
		Username: username,
	}, nil
}
