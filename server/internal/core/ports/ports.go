package ports

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
)

type (
	AppRepository interface {
		GetUser(ctx context.Context, userID uuid.UUID) (*domain.UserDB, error)
		GetUserByUsername(ctx context.Context, username string) (*domain.UserDB, error)
		CreateUser(ctx context.Context, user *domain.UserDB) (*domain.UserDB, error)

		GetPost(ctx context.Context, postID uuid.UUID) (*domain.PostDB, error)
		CreatePost(ctx context.Context, post *domain.PostDB) (*domain.PostDB, error)
		DeletePost(ctx context.Context, postID uuid.UUID) error

		GetAllPosts(ctx context.Context) ([]domain.PostDB, error)
		GetAllPostsByCategory(ctx context.Context, category string) ([]domain.PostDB, error)
		GetAllPostsByUser(ctx context.Context, userID uuid.UUID) ([]domain.PostDB, error)
		GetAllPostsByUsername(ctx context.Context, username string) ([]domain.PostDB, error)

		IncrementPostViews(ctx context.Context, postID uuid.UUID) error

		GetPostComments(ctx context.Context, postID uuid.UUID) ([]domain.CommentDB, error)

		GetPostComment(ctx context.Context, commentID uuid.UUID) (*domain.CommentDB, error)
		CreatePostComment(ctx context.Context, comment *domain.CommentDB) (*domain.CommentDB, error)
		DeletePostComment(ctx context.Context, commentID uuid.UUID) error

		GetPostVotes(ctx context.Context, postID uuid.UUID) ([]domain.VoteDB, error)

		UpvotePost(ctx context.Context, postID, userID uuid.UUID) error
		DownvotePost(ctx context.Context, postID, userID uuid.UUID) error
		UnvotePost(ctx context.Context, postID, userID uuid.UUID) error
	}

	AtomicAppRepository interface {
		Execute(ctx context.Context, fn func(ctx context.Context, repo AppRepository) error) error
	}

	AppRepositoryFactory       func() AppRepository
	AtomicAppRepositoryFactory func() AtomicAppRepository

	AppService interface {
		GetPost(ctx context.Context, postID uuid.UUID) (*domain.Post, error)
		CreatePost(ctx context.Context, req *domain.CreatePostRequest) (*domain.Post, error)
		DeletePost(ctx context.Context, postID uuid.UUID, userID uuid.UUID) error

		GetAllPosts(ctx context.Context) ([]domain.Post, error)
		GetAllPostsByCategory(ctx context.Context, category string) ([]domain.Post, error)
		GetAllPostsByUser(ctx context.Context, userID uuid.UUID) ([]domain.Post, error)
		GetAllPostsByUsername(ctx context.Context, username string) ([]domain.Post, error)

		CreatePostComment(ctx context.Context, req *domain.CreateCommentRequest) (*domain.Post, error)
		DeletePostComment(ctx context.Context, commentID uuid.UUID, userID uuid.UUID) (*domain.Post, error)

		UpvotePost(ctx context.Context, postID, userID uuid.UUID) (*domain.Post, error)
		DownvotePost(ctx context.Context, postID, userID uuid.UUID) (*domain.Post, error)
		UnvotePost(ctx context.Context, postID, userID uuid.UUID) (*domain.Post, error)
	}

	AuthService interface {
		Register(ctx context.Context, username, password string) (string, error)
		Login(ctx context.Context, username, password string) (string, error)
		NewJWT(user *domain.User) (string, error)
		ParseJWT(token string) (*domain.User, error)
	}
)

var (
	ErrUserNotFound    = fmt.Errorf("user not found")
	ErrPostNotFound    = fmt.Errorf("post not found")
	ErrCommentNotFound = fmt.Errorf("comment not found")

	ErrCannotBeginTransaction = fmt.Errorf("cannot begin transaction")
	ErrTransactionFailed      = fmt.Errorf("transaction failed")

	ErrTokenExpired      = fmt.Errorf("token expired")
	ErrTokenCreateFailed = fmt.Errorf("failed to create token")
	ErrTokenParseFailed  = fmt.Errorf("failed to parse token")
)
