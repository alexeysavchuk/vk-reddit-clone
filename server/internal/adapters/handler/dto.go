package handler

import (
	"fmt"
	"time"

	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
)

type (
	User struct {
		UserID   string `json:"id"`
		Username string `json:"username"`
	}

	Vote struct {
		UserID string `json:"user"`
		Value  int    `json:"vote"`
	}

	Comment struct {
		CommentID string `json:"id"`
		Author    User   `json:"author"`
		Body      string `json:"body"`
		CreatedAt string `json:"created"`
	}

	Post struct {
		PostID           string    `json:"id"`
		Author           User      `json:"author"`
		Title            string    `json:"title"`
		Category         string    `json:"category"`
		Type             string    `json:"type"`
		Views            int       `json:"views"`
		Comments         []Comment `json:"comments"`
		Votes            []Vote    `json:"votes"`
		Score            int       `json:"score"`
		UpvotePercentage int       `json:"upvotePercentage"`
		CreatedAt        string    `json:"created"`
	}

	TextPost struct {
		Post
		Body string `json:"text"`
	}

	LinkPost struct {
		Post
		Body string `json:"url"`
	}
)

func ToVoteDTO(vote *domain.Vote) Vote {
	return Vote{
		UserID: vote.UserID,
		Value:  vote.Value,
	}
}

func ToVotesDTO(votes []domain.Vote) []Vote {
	ret := make([]Vote, 0)
	for _, vote := range votes {
		ret = append(ret, ToVoteDTO(&vote))
	}
	return ret
}

func ToCommentDTO(comment *domain.Comment) Comment {
	return Comment{
		CommentID: comment.CommentID,
		Author: User{
			UserID:   comment.Author.UserID,
			Username: comment.Author.Username,
		},
		Body:      comment.Body,
		CreatedAt: comment.CreatedAt.Format(time.RFC3339),
	}
}

func ToCommentsDTO(comments []domain.Comment) []Comment {
	ret := make([]Comment, 0)
	for _, comment := range comments {
		ret = append(ret, ToCommentDTO(&comment))
	}
	return ret
}

func ToPostDTO(post *domain.Post) (any, error) {
	postDTO := Post{
		PostID:           post.PostID,
		Author:           User{UserID: post.Author.UserID, Username: post.Author.Username},
		Title:            post.Title,
		Category:         post.Category,
		Type:             post.Type,
		Views:            post.Views,
		Comments:         ToCommentsDTO(post.Comments),
		Votes:            ToVotesDTO(post.Votes),
		Score:            post.Score,
		UpvotePercentage: post.UpvotePercentage,
		CreatedAt:        post.CreatedAt.Format(time.RFC3339),
	}

	switch post.Type {
	case "text":
		return &TextPost{
			Post: postDTO,
			Body: post.Body,
		}, nil
	case "link":
		return &LinkPost{
			Post: postDTO,
			Body: post.Body,
		}, nil
	default:
		return nil, fmt.Errorf("unknown post type: %s", post.Type)
	}
}

func ToPostsDTO(posts []domain.Post) ([]any, error) {
	postsDTO := make([]any, 0, len(posts))
	for _, post := range posts {
		postDTO, err := ToPostDTO(&post)
		if err != nil {
			return nil, err
		}
		postsDTO = append(postsDTO, postDTO)
	}
	return postsDTO, nil
}

type AuthRequestBody struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type AuthResponseBody struct {
	Token string `json:"token"`
}

type CreateCommentRequestBody struct {
	Body string `json:"comment"`
}

type CreatePostRequestBody struct {
	Title    string `json:"title"`
	Category string `json:"category"`
	Type     string `json:"type"`
	TextBody string `json:"text,omitempty"`
	LinkBody string `json:"url,omitempty"`
}
