package handler

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/ports"
)

type AppHandlers struct {
	appService  ports.AppService
	authService ports.AuthService
}

func NewAppHandlers(
	appService ports.AppService,
	authService ports.AuthService,
) *AppHandlers {
	return &AppHandlers{
		appService:  appService,
		authService: authService,
	}
}

func (h *AppHandlers) Register(w http.ResponseWriter, r *http.Request) {
	dec := json.NewDecoder(r.Body)

	var user AuthRequestBody
	if err := dec.Decode(&user); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	token, err := h.authService.Register(r.Context(), user.Username, user.Password)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	enc := json.NewEncoder(w)
	if err := enc.Encode(AuthResponseBody{Token: token}); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *AppHandlers) Login(w http.ResponseWriter, r *http.Request) {
	dec := json.NewDecoder(r.Body)

	var user AuthRequestBody
	if err := dec.Decode(&user); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	token, err := h.authService.Login(r.Context(), user.Username, user.Password)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	enc := json.NewEncoder(w)
	if err := enc.Encode(AuthResponseBody{Token: token}); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *AppHandlers) GetPost(w http.ResponseWriter, r *http.Request) {
	enc := json.NewEncoder(w)

	postID := r.PathValue("postID")
	post, err := h.appService.GetPost(r.Context(), uuid.MustParse(postID))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	postDTO, err := ToPostDTO(post)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if err := enc.Encode(postDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *AppHandlers) CreatePost(w http.ResponseWriter, r *http.Request) {
	token := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

	if token == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("no token provided"))
		return
	}

	user, err := h.authService.ParseJWT(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}

	userID := uuid.MustParse(user.UserID)

	dec := json.NewDecoder(r.Body)
	var createRequest CreatePostRequestBody
	if err := dec.Decode(&createRequest); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var body string
	switch createRequest.Type {
	case "text":
		body = createRequest.TextBody
	case "link":
		body = createRequest.LinkBody
	default:
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	post, err := h.appService.CreatePost(r.Context(), &domain.CreatePostRequest{
		AuthorID: userID,
		Title:    createRequest.Title,
		Category: createRequest.Category,
		Type:     createRequest.Type,
		Body:     body,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	postDTO, err := ToPostDTO(post)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	enc := json.NewEncoder(w)
	if err := enc.Encode(postDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *AppHandlers) DeletePost(w http.ResponseWriter, r *http.Request) {
	token := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

	if token == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("no token provided"))
		return
	}

	user, err := h.authService.ParseJWT(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}

	userID := uuid.MustParse(user.UserID)
	postID := uuid.MustParse(r.PathValue("postID"))

	err = h.appService.DeletePost(r.Context(), postID, userID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("{ \"message\": \"success\" }"))
}

func (h *AppHandlers) GetAllPosts(w http.ResponseWriter, r *http.Request) {
	enc := json.NewEncoder(w)

	posts, err := h.appService.GetAllPosts(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	postsDTO, err := ToPostsDTO(posts)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if err := enc.Encode(postsDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *AppHandlers) GetAllPostsByCategory(w http.ResponseWriter, r *http.Request) {
	enc := json.NewEncoder(w)

	categoryName := r.PathValue("categoryName")
	posts, err := h.appService.GetAllPostsByCategory(r.Context(), categoryName)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	postsDTO, err := ToPostsDTO(posts)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if err := enc.Encode(postsDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *AppHandlers) GetAllPostsByUser(w http.ResponseWriter, r *http.Request) {
	username := r.PathValue("username")
	if username == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	posts, err := h.appService.GetAllPostsByUsername(r.Context(), username)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	postsDTO, err := ToPostsDTO(posts)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	enc := json.NewEncoder(w)
	if err := enc.Encode(postsDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *AppHandlers) CreateComment(w http.ResponseWriter, r *http.Request) {
	token := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

	if token == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("no token provided"))
		return
	}

	user, err := h.authService.ParseJWT(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}

	userID := uuid.MustParse(user.UserID)
	postID := uuid.MustParse(r.PathValue("postID"))

	dec := json.NewDecoder(r.Body)
	var createRequest CreateCommentRequestBody
	err = dec.Decode(&createRequest)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	post, err := h.appService.CreatePostComment(r.Context(), &domain.CreateCommentRequest{
		PostID:   postID,
		AuthorID: userID,
		Body:     createRequest.Body,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	postDTO, err := ToPostDTO(post)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	enc := json.NewEncoder(w)
	if err := enc.Encode(postDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *AppHandlers) DeleteComment(w http.ResponseWriter, r *http.Request) {
	token := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

	if token == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("no token provided"))
		return
	}

	user, err := h.authService.ParseJWT(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}

	userID := uuid.MustParse(user.UserID)
	commentID := uuid.MustParse(r.PathValue("commentID"))

	post, err := h.appService.DeletePostComment(r.Context(), commentID, userID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	postDTO, err := ToPostDTO(post)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	enc := json.NewEncoder(w)
	if err := enc.Encode(postDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *AppHandlers) UpvotePost(w http.ResponseWriter, r *http.Request) {
	token := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

	if token == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("no token provided"))
		return
	}

	user, err := h.authService.ParseJWT(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}

	postID := uuid.MustParse(r.PathValue("postID"))
	userID := uuid.MustParse(user.UserID)

	post, err := h.appService.UpvotePost(r.Context(), postID, userID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("failed to upvote post"))
		return
	}

	postDTO, err := ToPostDTO(post)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("failed to upvote post"))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	enc := json.NewEncoder(w)
	if err := enc.Encode(postDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("failed to upvote post"))
		return
	}
}

func (h *AppHandlers) DownvotePost(w http.ResponseWriter, r *http.Request) {
	token := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

	if token == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("no token provided"))
		return
	}

	user, err := h.authService.ParseJWT(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("invalid token"))
		return
	}

	postID := uuid.MustParse(r.PathValue("postID"))
	userID := uuid.MustParse(user.UserID)

	post, err := h.appService.DownvotePost(r.Context(), postID, userID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("unable to downvote post"))
		return
	}

	postDTO, err := ToPostDTO(post)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("unable to downvote post"))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	enc := json.NewEncoder(w)
	if err := enc.Encode(postDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("unable to downvote post"))
		return
	}
}

func (h *AppHandlers) UnvotePost(w http.ResponseWriter, r *http.Request) {
	token := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")

	if token == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("no token provided"))
		return
	}

	user, err := h.authService.ParseJWT(token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("invalid token"))
		return
	}

	postID := uuid.MustParse(r.PathValue("postID"))
	userID := uuid.MustParse(user.UserID)

	post, err := h.appService.UnvotePost(r.Context(), postID, userID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("failed to unvote post"))
		return
	}

	postDTO, err := ToPostDTO(post)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("failed to unvote post"))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	enc := json.NewEncoder(w)
	if err := enc.Encode(postDTO); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("failed to unvote post"))
		return
	}
}
