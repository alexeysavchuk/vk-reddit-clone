package repository

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/ports"
)

type DB interface {
	Exec(query string, args ...any) (sql.Result, error)
	ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)

	Query(query string, args ...any) (*sql.Rows, error)
	QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error)

	QueryRow(query string, args ...any) *sql.Row
	QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
}

type AppRepository struct {
	db DB
}

func NewAppRepository(db DB) *AppRepository {
	return &AppRepository{db: db}
}

var _ ports.AppRepository = (*AppRepository)(nil)

func (r *AppRepository) GetUser(ctx context.Context, userID uuid.UUID) (*domain.UserDB, error) {
	row := r.db.QueryRowContext(ctx, queryGetUser, userID)
	err := row.Err()
	switch {
	case err == sql.ErrNoRows:
		return nil, fmt.Errorf("user with id %s not found", userID)
	case err != nil:
		return nil, fmt.Errorf("failed to get user with id %s: %w", userID, err)
	default:
		var user domain.UserDB
		if err := row.Scan(&user.UserID, &user.Username, &user.Password); err != nil {
			return nil, fmt.Errorf("failed to scan user with id %s: %w", userID, err)
		}
		return &user, nil
	}
}

func (r *AppRepository) GetUserByUsername(ctx context.Context, username string) (*domain.UserDB, error) {
	row := r.db.QueryRowContext(ctx, queryGetUserByUsername, username)
	err := row.Err()
	switch {
	case err == sql.ErrNoRows:
		return nil, fmt.Errorf("user with username %s not found", username)
	case err != nil:
		return nil, fmt.Errorf("failed to get user with username %s: %w", username, err)
	default:
		var user domain.UserDB
		if err := row.Scan(
			&user.UserID,
			&user.Username,
			&user.Password,
		); err != nil {
			return nil, fmt.Errorf("failed to scan user with username %s: %w", username, err)
		}
		return &user, nil
	}
}

func (r *AppRepository) CreateUser(ctx context.Context, user *domain.UserDB) (*domain.UserDB, error) {
	row := r.db.QueryRowContext(ctx, queryCreateUser, user.Username, user.Password)
	err := row.Err()
	switch {
	case err == sql.ErrNoRows || err != nil:
		return nil, fmt.Errorf("failed to create user: %w", err)
	default:
		var newUser domain.UserDB
		if err := row.Scan(
			&newUser.UserID,
			&newUser.Username,
			&newUser.Password,
		); err != nil {
			return nil, fmt.Errorf("failed to scan user: %w", err)
		}
		return &newUser, nil
	}
}

func (r *AppRepository) GetPost(ctx context.Context, postID uuid.UUID) (*domain.PostDB, error) {
	row := r.db.QueryRowContext(ctx, queryGetPost, postID)
	err := row.Err()
	switch {
	case err == sql.ErrNoRows:
		return nil, fmt.Errorf("post with id %s not found", postID)
	case err != nil:
		return nil, fmt.Errorf("failed to get post with id %s: %w", postID, err)
	default:
		var post domain.PostDB
		if err := row.Scan(
			&post.PostID,
			&post.AuthorID,
			&post.Title,
			&post.Category,
			&post.Type,
			&post.Body,
			&post.Views,
			&post.CreatedAt,
		); err != nil {
			return nil, fmt.Errorf("failed to scan post with id %s: %w", postID, err)
		}
		return &post, nil
	}
}

func (r *AppRepository) CreatePost(ctx context.Context, post *domain.PostDB) (*domain.PostDB, error) {
	row := r.db.QueryRowContext(ctx, queryCreatePost,
		post.AuthorID,
		post.Title,
		post.Category,
		post.Type,
		post.Body,
	)
	err := row.Err()
	switch {
	case err == sql.ErrNoRows || err != nil:
		return nil, fmt.Errorf("failed to create post: %w", err)
	default:
		var newPost domain.PostDB
		if err := row.Scan(
			&newPost.PostID,
			&newPost.AuthorID,
			&newPost.Title,
			&newPost.Category,
			&newPost.Type,
			&newPost.Body,
			&newPost.Views,
			&newPost.CreatedAt,
		); err != nil {
			return nil, fmt.Errorf("failed to scan post: %w", err)
		}
		return &newPost, nil
	}
}

func (r *AppRepository) DeletePost(ctx context.Context, postID uuid.UUID) error {
	_, err := r.db.ExecContext(ctx, queryDeletePost, postID)
	if err != nil {
		return fmt.Errorf("failed to delete post %s: %w", postID, err)
	}
	return nil
}

func (r *AppRepository) GetAllPosts(ctx context.Context) ([]domain.PostDB, error) {
	rows, err := r.db.QueryContext(ctx, queryGetAllPosts)
	if err != nil {
		return nil, fmt.Errorf("failed to get all posts: %w", err)
	}
	defer rows.Close()

	posts := make([]domain.PostDB, 0)
	for rows.Next() {
		var post domain.PostDB
		if err := rows.Scan(
			&post.PostID,
			&post.AuthorID,
			&post.Title,
			&post.Category,
			&post.Type,
			&post.Body,
			&post.Views,
			&post.CreatedAt,
		); err != nil {
			return nil, fmt.Errorf("failed to scan post: %w", err)
		}
		posts = append(posts, post)
	}

	return posts, nil
}

func (r *AppRepository) GetAllPostsByCategory(ctx context.Context, category string) ([]domain.PostDB, error) {
	rows, err := r.db.QueryContext(ctx, queryGetAllPostsByCategory, category)
	if err != nil {
		return nil, fmt.Errorf("failed to get posts by category: %w", err)
	}
	defer rows.Close()

	posts := make([]domain.PostDB, 0)
	for rows.Next() {
		var post domain.PostDB
		if err := rows.Scan(
			&post.PostID,
			&post.AuthorID,
			&post.Title,
			&post.Category,
			&post.Type,
			&post.Body,
			&post.Views,
			&post.CreatedAt,
		); err != nil {
			return nil, fmt.Errorf("failed to scan post: %w", err)
		}
		posts = append(posts, post)
	}

	return posts, nil
}

func (r *AppRepository) GetAllPostsByUser(ctx context.Context, userID uuid.UUID) ([]domain.PostDB, error) {
	rows, err := r.db.QueryContext(ctx, queryGetAllPostsByUser, userID)
	if err != nil {
		return nil, fmt.Errorf("failed to get posts by user: %w", err)
	}
	defer rows.Close()

	posts := make([]domain.PostDB, 0)
	for rows.Next() {
		var post domain.PostDB
		if err := rows.Scan(
			&post.PostID,
			&post.AuthorID,
			&post.Title,
			&post.Category,
			&post.Type,
			&post.Body,
			&post.Views,
			&post.CreatedAt,
		); err != nil {
			return nil, fmt.Errorf("failed to scan post: %w", err)
		}
		posts = append(posts, post)
	}

	return posts, nil
}

func (r *AppRepository) GetAllPostsByUsername(ctx context.Context, username string) ([]domain.PostDB, error) {
	rows, err := r.db.QueryContext(ctx, queryGetAllPostsByUsername, username)
	if err != nil {
		return nil, fmt.Errorf("failed to get posts by username: %w", err)
	}
	defer rows.Close()

	posts := make([]domain.PostDB, 0)
	for rows.Next() {
		var post domain.PostDB
		if err := rows.Scan(
			&post.PostID,
			&post.AuthorID,
			&post.Title,
			&post.Category,
			&post.Type,
			&post.Body,
			&post.Views,
			&post.CreatedAt,
		); err != nil {
			return nil, fmt.Errorf("failed to scan post: %w", err)
		}
		posts = append(posts, post)
	}

	return posts, nil
}

func (r *AppRepository) IncrementPostViews(ctx context.Context, postID uuid.UUID) error {
	_, err := r.db.ExecContext(ctx, queryIncrementPostViews, postID)
	if err != nil {
		return fmt.Errorf("failed to increment post views: %w", err)
	}
	return nil
}

func (r *AppRepository) GetPostComments(ctx context.Context, postID uuid.UUID) ([]domain.CommentDB, error) {
	rows, err := r.db.QueryContext(ctx, queryGetPostComments, postID)
	if err != nil {
		return nil, fmt.Errorf("failed to get comments: %w", err)
	}
	defer rows.Close()

	comments := make([]domain.CommentDB, 0)
	for rows.Next() {
		var comment domain.CommentDB
		if err := rows.Scan(
			&comment.CommentID,
			&comment.PostID,
			&comment.AuthorID,
			&comment.Body,
			&comment.CreatedAt,
		); err != nil {
			return nil, fmt.Errorf("failed to scan comment: %w", err)
		}
		comments = append(comments, comment)
	}

	return comments, nil
}

func (r *AppRepository) GetPostComment(ctx context.Context, commentID uuid.UUID) (*domain.CommentDB, error) {
	row := r.db.QueryRowContext(ctx, queryGetPostComment, commentID)
	err := row.Err()

	switch {
	case err == sql.ErrNoRows || err != nil:
		return nil, fmt.Errorf("failed to get comment: %w", err)
	default:
		var comment domain.CommentDB
		if err := row.Scan(
			&comment.CommentID,
			&comment.PostID,
			&comment.AuthorID,
			&comment.Body,
			&comment.CreatedAt,
		); err != nil {
			return nil, fmt.Errorf("failed to scan comment: %w", err)
		}
		return &comment, nil
	}
}

func (r *AppRepository) CreatePostComment(ctx context.Context, comment *domain.CommentDB) (*domain.CommentDB, error) {
	row := r.db.QueryRowContext(ctx, queryCreatePostComment,
		comment.PostID,
		comment.AuthorID,
		comment.Body,
	)
	err := row.Err()
	switch {
	case err == sql.ErrNoRows || err != nil:
		return nil, fmt.Errorf("failed to create comment: %w", err)
	default:
		var newComment domain.CommentDB
		if err := row.Scan(
			&newComment.CommentID,
			&newComment.PostID,
			&newComment.AuthorID,
			&newComment.Body,
			&newComment.CreatedAt,
		); err != nil {
			return nil, fmt.Errorf("failed to scan comment: %w", err)
		}
		return &newComment, nil
	}
}

func (r *AppRepository) DeletePostComment(ctx context.Context, commentID uuid.UUID) error {
	_, err := r.db.ExecContext(ctx, queryDeletePostComment, commentID)
	if err != nil {
		return fmt.Errorf("failed to delete comment %s: %w", commentID, err)
	}
	return nil
}

func (r *AppRepository) GetPostVotes(ctx context.Context, postID uuid.UUID) ([]domain.VoteDB, error) {
	rows, err := r.db.QueryContext(ctx, queryGetPostVotes, postID)
	if err != nil {
		return nil, fmt.Errorf("failed to get votes of post %s: %w", postID, err)
	}
	defer rows.Close()

	votes := make([]domain.VoteDB, 0)
	for rows.Next() {
		var vote domain.VoteDB
		if err := rows.Scan(
			&vote.VoteID,
			&vote.PostID,
			&vote.UserID,
			&vote.Value,
		); err != nil {
			return nil, fmt.Errorf("failed to scan vote: %w", err)
		}
		votes = append(votes, vote)
	}

	return votes, nil
}

func (r *AppRepository) UpvotePost(ctx context.Context, postID, userID uuid.UUID) error {
	_, err := r.db.ExecContext(ctx, queryUpvotePost, postID, userID)
	if err != nil {
		return fmt.Errorf("failed to upvote post %s: %w", postID, err)
	}
	return nil
}

func (r *AppRepository) DownvotePost(ctx context.Context, postID, userID uuid.UUID) error {
	_, err := r.db.ExecContext(ctx, queryDownvotePost, postID, userID)
	if err != nil {
		return fmt.Errorf("failed to downvote post %s: %w", postID, err)
	}
	return nil
}

func (r *AppRepository) UnvotePost(ctx context.Context, postID, userID uuid.UUID) error {
	_, err := r.db.ExecContext(ctx, queryUnvotePost, postID, userID)
	if err != nil {
		return fmt.Errorf("failed to unvote post %s: %w", postID, err)
	}
	return nil
}

type DBTransactor interface {
	Begin() (*sql.Tx, error)
	BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error)
	DB
}

type AtomicAppRepository struct {
	db DBTransactor
}

var _ ports.AtomicAppRepository = (*AtomicAppRepository)(nil)

func NewAtomicAppRepository(db DBTransactor) *AtomicAppRepository {
	return &AtomicAppRepository{db: db}
}

func (r *AtomicAppRepository) Execute(
	ctx context.Context,
	fn func(ctx context.Context, repo ports.AppRepository) error,
) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %w", ports.ErrCannotBeginTransaction, err)
	}
	defer func() {
		if err := tx.Rollback(); err != nil {
			return
		}
	}()

	repo := NewAppRepository(tx)
	if err := fn(ctx, repo); err != nil {
		return fmt.Errorf("%w: %w", ports.ErrTransactionFailed, err)
	}
	if err := tx.Commit(); err != nil {
		return fmt.Errorf("%w: %w", ports.ErrTransactionFailed, err)
	}
	return nil
}
