package repository

const (
	queryGetPost = `
SELECT
	p."post_id",
	p."author_id",
	p."title",
	p."category",
	p."type",
	p."body",
	p."views",
	p."created_at"
FROM
	"post" p
WHERE
	p."post_id" = $1;
`

	queryCreatePost = `
INSERT INTO
	"post"
	(
		"author_id",
		"title",
		"category",
		"type",
		"body"
	)
VALUES
	(
		$1,
		$2,
		$3,
		$4,
		$5
	)
RETURNING
	"post_id",
	"author_id",
	"title",
	"category",
	"type",
	"body",
	"views",
	"created_at";
`

	queryDeletePost = `
DELETE FROM
	"post"
WHERE
	"post_id" = $1;
`

	queryGetAllPosts = `
SELECT
	p."post_id",
	p."author_id",
	p."title",
	p."category",
	p."type",
	p."body",
	p."views",
	p."created_at"
FROM
	"post" p
ORDER BY
	p."created_at" DESC;
`

	queryGetAllPostsByCategory = `
SELECT
	p."post_id",
	p."author_id",
	p."title",
	p."category",
	p."type",
	p."body",
	p."views",
	p."created_at"
FROM
	"post" p
WHERE
	p."category" = $1
ORDER BY
	p."created_at" DESC;
`

	queryGetAllPostsByUser = `
SELECT
	p."post_id",
	p."author_id",
	p."title",
	p."category",
	p."type",
	p."body",
	p."views",
	p."created_at"
FROM
	"post" p
WHERE
	p."author_id" = $1
ORDER BY
	p."created_at" DESC;
`

	queryGetAllPostsByUsername = `
SELECT
	p."post_id",
	p."author_id",
	p."title",
	p."category",
	p."type",
	p."body",
	p."views",
	p."created_at"
FROM
	"post" p
JOIN
	"user" u
ON
  	p."author_id" = u."user_id"
WHERE
	u."username" = $1
ORDER BY
	p."created_at" DESC;
`

	queryIncrementPostViews = `
UPDATE
	"post"
SET
	"views" = "views" + 1
WHERE
	"post_id" = $1;
`

	queryGetPostComments = `
SELECT
	c."comment_id",
	c."post_id",
	c."author_id",
	c."body",
	c."created_at"
FROM
	"comment" c
WHERE
	c."post_id" = $1
ORDER BY
	c."created_at" DESC;
`

	queryGetPostComment = `
SELECT
	c."comment_id",
	c."post_id",
	c."author_id",
	c."body",
	c."created_at"
FROM
	"comment" c
WHERE
	c."comment_id" = $1;
`

	queryCreatePostComment = `
INSERT INTO
	comment
	(
		"post_id",
		"author_id",
		"body"
	)
VALUES
	(
		$1,
		$2,
		$3
	)
RETURNING
	"comment_id",
	"post_id",
	"author_id",
	"body",
	"created_at";
`

	queryDeletePostComment = `
DELETE FROM
	"comment" c
WHERE
	c."comment_id" = $1;
`

	queryGetPostVotes = `
SELECT
	v."vote_id",
	v."post_id",
	v."user_id",
	v."value"
FROM
	"vote" v
WHERE
	v."post_id" = $1;
`

	queryUpvotePost = `
INSERT INTO
	"vote"
	(
		"post_id",
		"user_id",
		"value"
	)
VALUES
	(
		$1,
		$2,
		1
);
`

	queryDownvotePost = `
INSERT INTO
	"vote"
	(
		"post_id",
		"user_id",
		"value"
	)
VALUES
	(
		$1,
		$2,
		-1
	);
`

	queryUnvotePost = `
DELETE FROM
	"vote" v
WHERE
	v."post_id" = $1 AND
	v."user_id" = $2;
`

	queryGetUser = `
SELECT
	u."user_id",
	u."username",
	u."password"
FROM
	"user" u
WHERE
	u."user_id" = $1;
`

	queryGetUserByUsername = `
SELECT
	u."user_id",
	u."username",
	u."password"
FROM
	"user" u
WHERE
	u."username" = $1;
`

	queryCreateUser = `
INSERT INTO
	"user"
	(
		"username",
		"password"
	)
VALUES
	(
		$1,
		$2
	)
RETURNING
	"user_id",
	"username",
	"password";
`
)
