package faker

import (
	"math/rand"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/google/uuid"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
)

func getRandomFrom(arr []any) any {
	return arr[rand.Intn(len(arr))]
}

func getFakeTitle() string {
	return gofakeit.HipsterSentence(rand.Intn(5) + 2)
}

func getFakeCategory() string {
	return getRandomFrom([]any{
		"music",
		"funny",
		"videos",
		"programming",
		"news",
		"fashion",
	}).(string)
}

func getFakeBody() string {
	return gofakeit.HipsterParagraph(
		rand.Intn(3)+1,
		rand.Intn(3)+1,
		rand.Intn(10)+5,
		"\n",
	)
}

func getFakeViews() int {
	return rand.Intn(1000) + 100
}

func GetFakeUser() *domain.UserDB {
	return &domain.UserDB{
		UserID:   uuid.New(),
		Username: gofakeit.Username(),
		Password: gofakeit.Password(true, true, true, true, false, 20),
	}
}

func GetFakePost(authorID uuid.UUID) *domain.PostDB {
	postType := getRandomFrom([]any{"text", "link"}).(string)

	switch postType {
	case "text":
		return &domain.PostDB{
			PostID:    uuid.New(),
			AuthorID:  authorID,
			Title:     getFakeTitle(),
			Category:  getFakeCategory(),
			Type:      "text",
			Body:      getFakeBody(),
			Views:     getFakeViews(),
			CreatedAt: gofakeit.PastDate(),
		}
	case "link":
		return &domain.PostDB{
			PostID:    uuid.New(),
			AuthorID:  authorID,
			Title:     getFakeTitle(),
			Category:  getFakeCategory(),
			Type:      "link",
			Body:      gofakeit.URL(),
			Views:     getFakeViews(),
			CreatedAt: gofakeit.PastDate(),
		}
	default:
		panic("unknown post type")
	}
}

func GetFakeComment(postID uuid.UUID, authorID uuid.UUID) *domain.CommentDB {
	return &domain.CommentDB{
		CommentID: uuid.New(),
		PostID:    postID,
		AuthorID:  authorID,
		Body:      getFakeBody(),
	}
}

func GetFakeVote(postID uuid.UUID, userID uuid.UUID) *domain.VoteDB {
	return &domain.VoteDB{
		VoteID: uuid.New(),
		PostID: postID,
		UserID: userID,
		Value:  getRandomFrom([]any{1, -1}).(int),
	}
}

func ChooseRandomUser(users []*domain.UserDB) *domain.UserDB {
	return users[rand.Intn(len(users))]
}

func ChooseRandomPost(posts []*domain.PostDB) *domain.PostDB {
	return posts[rand.Intn(len(posts))]
}

func ChooseRandomComment(comments []*domain.CommentDB) *domain.CommentDB {
	return comments[rand.Intn(len(comments))]
}

func ChooseRandomVote(votes []*domain.VoteDB) *domain.VoteDB {
	return votes[rand.Intn(len(votes))]
}
