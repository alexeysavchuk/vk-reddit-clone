package main

import (
	"database/sql"
	"net/http"
	"os"

	_ "github.com/lib/pq"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/adapters/handler"
	repository "gitlab.com/alexeysavchuk/vk-reddit-clone/internal/adapters/repository/postgres"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/ports"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/services"
)

func main() {
	url := os.Getenv("DATABASE_URL")

	db, err := sql.Open("postgres", url)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	appService := services.NewAppService(
		func() ports.AppRepository {
			return repository.NewAppRepository(db)
		},
		func() ports.AtomicAppRepository {
			return repository.NewAtomicAppRepository(db)
		},
	)
	authService := services.NewAuthService(
		func() ports.AppRepository {
			return repository.NewAppRepository(db)
		},
		func() ports.AtomicAppRepository {
			return repository.NewAtomicAppRepository(db)
		},
	)
	handlers := handler.NewAppHandlers(
		appService,
		authService,
	)

	mux := http.NewServeMux()

	mux.HandleFunc("POST /api/register", handlers.Register)
	mux.HandleFunc("POST /api/login", handlers.Login)

	mux.HandleFunc("GET /api/post/{postID}", handlers.GetPost)
	mux.HandleFunc("POST /api/posts", handlers.CreatePost)
	mux.HandleFunc("DELETE /api/post/{postID}", handlers.DeletePost)

	mux.HandleFunc("GET /api/posts/", handlers.GetAllPosts)
	mux.HandleFunc("GET /api/posts/{categoryName}/", handlers.GetAllPostsByCategory)
	mux.HandleFunc("GET /api/user/{username}/", handlers.GetAllPostsByUser)

	mux.HandleFunc("POST /api/post/{postID}", handlers.CreateComment)
	mux.HandleFunc("DELETE /api/post/{postID}/{commentID}", handlers.DeleteComment)

	mux.HandleFunc("GET /api/post/{postID}/upvote", handlers.UpvotePost)
	mux.HandleFunc("GET /api/post/{postID}/downvote", handlers.DownvotePost)
	mux.HandleFunc("GET /api/post/{postID}/unvote", handlers.UnvotePost)

	server := http.Server{
		Addr:    ":8080",
		Handler: mux,
	}
	server.ListenAndServe()
}
