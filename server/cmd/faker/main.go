package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/common/faker"
	"gitlab.com/alexeysavchuk/vk-reddit-clone/internal/core/domain"
)

func main() {
	var host string
	var port string
	var user string
	var dbName string
	var needPassword bool

	var numUsers int
	var numPosts int
	var numComments int
	var numVotes int

	flag.StringVar(&host, "host", "localhost", "database host")
	flag.StringVar(&port, "port", "5432", "database port")
	flag.StringVar(&user, "user", "", "database user")
	flag.StringVar(&dbName, "db", "", "database name")
	flag.BoolVar(&needPassword, "pwd", false, "database password")

	flag.IntVar(&numUsers, "n_user", 100, "number of users")
	flag.IntVar(&numPosts, "n_post", 500, "number of posts")
	flag.IntVar(&numComments, "n_comment", 1000, "number of comments")
	flag.IntVar(&numVotes, "n_vote", 10000, "number of votes")

	flag.Parse()

	var password string
	if needPassword {
		fmt.Printf("Enter password: ")
		_, err := fmt.Scan(&password)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to read password: %v\n", err)
			os.Exit(1)
		}
	}

	url := fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		user, password, host, port, dbName,
	)

	db, err := getDB(url)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer db.Close()

	fmt.Println("Successfully connected!")

	err = PopulateDB(
		db,
		PopulateConfig{
			numUsers,
			numPosts,
			numComments,
			numVotes,
		},
	)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to populate database: %v\n", err)
		os.Exit(1)
	}

	fmt.Println("Successfully populated!")
}

func getDB(url string) (*sql.DB, error) {
	db, err := sql.Open("postgres", url)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}

type PopulateConfig struct {
	Users    int
	Posts    int
	Comments int
	Votes    int
}

func PopulateDB(db *sql.DB, cfg PopulateConfig) error {
	fmt.Printf(
		"Populating with %d users, %d posts, %d comments, %d votes\n",
		cfg.Users, cfg.Posts, cfg.Comments, cfg.Votes,
	)

	fakeUsers := make([]*domain.UserDB, 0, cfg.Users)
	for i := 0; i < cfg.Users; i++ {
		fakeUsers = append(fakeUsers, faker.GetFakeUser())
	}

	fakePosts := make([]*domain.PostDB, 0, cfg.Posts)
	for i := 0; i < cfg.Posts; i++ {
		authorID := fakeUsers[rand.Intn(len(fakeUsers))].UserID
		fakePosts = append(fakePosts, faker.GetFakePost(authorID))
	}

	fakeComments := make([]*domain.CommentDB, 0, cfg.Comments)
	for i := 0; i < cfg.Comments; i++ {
		postID := fakePosts[rand.Intn(len(fakePosts))].PostID
		authorID := fakeUsers[rand.Intn(len(fakeUsers))].UserID
		fakeComments = append(fakeComments, faker.GetFakeComment(postID, authorID))
	}

	fakeVotes := make([]*domain.VoteDB, 0, cfg.Votes)
	for i := 0; i < cfg.Votes; i++ {
		postID := fakePosts[rand.Intn(len(fakePosts))].PostID
		userID := fakeUsers[rand.Intn(len(fakeUsers))].UserID
		fakeVotes = append(fakeVotes, faker.GetFakeVote(postID, userID))
	}

	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)
	tx, err := db.BeginTx(ctx, nil)
	defer tx.Rollback()

	if err != nil {
		return fmt.Errorf("failed to begin transaction: %w", err)
	}

	for _, user := range fakeUsers {
		err = insertUser(ctx, tx, user)
		if err != nil {
			return fmt.Errorf("failed to insert user: %w", err)
		}
	}

	for _, post := range fakePosts {
		err = insertPost(ctx, tx, post)
		if err != nil {
			return fmt.Errorf("failed to insert post: %w", err)
		}
	}

	for _, comment := range fakeComments {
		err = insertComment(ctx, tx, comment)
		if err != nil {
			return fmt.Errorf("failed to insert comment: %w", err)
		}
	}

	for _, vote := range fakeVotes {
		err = insertVote(ctx, tx, vote)
		if err != nil {
			return fmt.Errorf("failed to insert vote: %w", err)
		}
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("failed to commit transaction: %w", err)
	}

	return nil
}

func insertUser(ctx context.Context, tx *sql.Tx, user *domain.UserDB) error {
	query := `
INSERT INTO
	"user"
	(
		"user_id",
		"username",
		"password"
	)
VALUES
	(
		$1,
		$2,
		$3
	);
`
	_, err := tx.ExecContext(ctx, query,
		user.UserID,
		user.Username,
		user.Password,
	)
	return err
}

func insertPost(ctx context.Context, tx *sql.Tx, post *domain.PostDB) error {
	query := `
INSERT INTO
	"post"
	(
		"post_id",
		"author_id",
		"title",
		"category",
		"type",
		"body",
		"views",
		"created_at"
)
VALUES
	(
		$1,
		$2,
		$3,
		$4,
		$5,
		$6,
		$7,
		$8
	);
`
	_, err := tx.ExecContext(ctx, query,
		post.PostID,
		post.AuthorID,
		post.Title,
		post.Category,
		post.Type,
		post.Body,
		post.Views,
		post.CreatedAt,
	)
	return err
}

func insertComment(ctx context.Context, tx *sql.Tx, comment *domain.CommentDB) error {
	query := `
INSERT INTO
	"comment"
	(
		"comment_id",
		"post_id",
		"author_id",
		"body"
	)
VALUES
	(
		$1,
		$2,
		$3,
		$4
	);
`
	_, err := tx.ExecContext(ctx, query,
		comment.CommentID,
		comment.PostID,
		comment.AuthorID,
		comment.Body,
	)
	return err
}

func insertVote(ctx context.Context, tx *sql.Tx, vote *domain.VoteDB) error {
	query := `
INSERT INTO
	"vote"
	(
		"vote_id",
		"post_id",
		"user_id",
		"value"
	)
VALUES
	(
		$1,
		$2,
		$3,
		$4
	);
`
	_, err := tx.ExecContext(ctx, query,
		vote.VoteID,
		vote.PostID,
		vote.UserID,
		vote.Value,
	)
	return err
}
