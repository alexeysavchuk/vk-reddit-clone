CREATE TABLE "user" (
    "user_id" UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    "username" VARCHAR(255) NOT NULL UNIQUE,
    "password" VARCHAR(255) NOT NULL
);
