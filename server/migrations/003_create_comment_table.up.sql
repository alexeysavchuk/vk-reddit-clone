CREATE TABLE "comment" (
    "comment_id" UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    "post_id"    UUID REFERENCES "post"("post_id") NOT NULL,
    "author_id"  UUID REFERENCES "user"("user_id") NOT NULL,
    "body"    TEXT NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);
