CREATE TABLE "post" (
    "post_id"    UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    "author_id"  UUID REFERENCES "user"("user_id") NOT NULL,
    "title"      VARCHAR(255) NOT NULL,
    "category"   VARCHAR(255) NOT NULL,
    "type"       VARCHAR(255) NOT NULL,
    "body"       TEXT NOT NULL,
    "views"      INT DEFAULT 0,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);
