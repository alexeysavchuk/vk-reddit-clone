CREATE TABLE "vote" (
    "vote_id" UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    "post_id" UUID REFERENCES "post"("post_id") NOT NULL,
    "user_id" UUID REFERENCES "user"("user_id") NOT NULL,
    "value" INTEGER NOT NULL CHECK ("value" IN (1, -1))
);
